# Dolly Zoom



## About

This addon is intended to assist the process of camera matching by providing an operator called 'Dolly Zoom' which can be used to dolly the active camera whilst simultaneously adjusting the focal length in order to maintain the compostition of the image.  The dolly zoom operates relative to the 3D cursor.  Before using the operator, place the 3D cursor at the location in your scene where you would like the compostion to stay the same.

You can find a guide to using Dolly Zoom here:
https://youtu.be/Ml3vzaqSD-g
