bl_info = {
    "name": "Dolly Zoom",
    "blender": (4, 1, 0),
    "category": "Camera",
}

import bpy
import math
import numpy as np
import mathutils

def calculate_angle(vector_a, vector_b):
        # Convert input vectors to numpy arrays
        a = np.array(vector_a)
        b = np.array(vector_b)
        
        # Calculate the dot product
        dot_product = np.dot(a, b)
        
        # Calculate the magnitudes of the vectors
        magnitude_a = np.linalg.norm(a)
        magnitude_b = np.linalg.norm(b)
        
        # Calculate the cosine of the angle
        cos_theta = dot_product / (magnitude_a * magnitude_b)
        
        # Calculate the angle in radians
        angle_radians = np.arccos(cos_theta)
        
        # Convert the angle to degrees
        angle_degrees = np.degrees(angle_radians)
        
        return angle_radians

class DollyZoom(bpy.types.Operator):
    """Dolly Zoom Operator"""
    bl_idname = "camera.dolly_zoom"
    bl_label = "Dolly Zoom"
    bl_options = {'REGISTER', 'UNDO'}
    
    def modal(self, context, event):
        
        # Get the camera
        cam = context.scene.camera
        
        # Get the 3D cursor location
        cursor_location = context.scene.cursor.location

        # get the direction cming out from the centre of camera
        direction = cam.matrix_world.to_3x3().col[2].normalized()

        # Calculate the distance between the camera and the 3D cursor
        initial_distance = (cam.location - cursor_location).length

        virtual_pivot = cam.location - (initial_distance * direction)

        vector_a = cam.location - cursor_location
        vector_b = cam.location - virtual_pivot

        angle = calculate_angle(vector_a, vector_b)
        adjusted_distance = (math.cos(angle) * vector_a).length
        adjusted_virtual_pivot = cam.location - (adjusted_distance * direction)

        if event.type == 'MOUSEMOVE':            

            if cam is None or cam.type != 'CAMERA':
                self.report({'WARNING'}, "No active camera found")
                return {'CANCELLED'}
            
            sensitivity = 1.0
            if event.shift:
                sensitivity = 0.1  # Adjust sensitivity when Shift key is pressed            

            # Calculate the distance between the camera and the 3D cursor
            distance = (cam.location - cursor_location).length

            # Change distance based on mouse movement
            delta = event.mouse_x - self.init_mouse_x
            new_distance = self.init_distance + delta * 0.1 * sensitivity

            # Avoid negative distance
            new_distance = max(new_distance, 1)

            # Calculate new focal length to maintain the perceived size
            new_focal_length = (new_distance / self.init_distance) * self.init_focal_length
            
            # Update the camera properties
            cam.data.lens = new_focal_length 
            
            # finally set camera location
            cam.location = adjusted_virtual_pivot + direction * new_distance
            return {'RUNNING_MODAL'}

        elif event.type in {'RET','LEFTMOUSE'}:
            return {'FINISHED'}
        
        elif event.type in {'ESC','RIGHTMOUSE'}:
            # Restore the original location of the camera
            self.restore_camera(context)
            return {'CANCELLED'}

        return {'PASS_THROUGH'}

    def invoke(self, context, event):

        # Store the original camera parameters
        self.original_location = context.scene.camera.location.copy()
        self.original_lens = context.scene.camera.data.lens
        # Get the 3D cursor location
        cursor_location = context.scene.cursor.location
        # Calculate the distance between the camera and the 3D cursor
        self.original_distance = (context.scene.camera.location - cursor_location).length

        self.init_mouse_x = event.mouse_x

        # Get the camera
        cam = context.scene.camera
        if cam is None or cam.type != 'CAMERA':
            self.report({'WARNING'}, "No active camera found")
            return {'CANCELLED'}

        # Get the 3D cursor location
        cursor_location = context.scene.cursor.location

        # Calculate the initial distance
        #cam_direction = cam.matrix_world.to_3x3().col[2].normalized()
        #self.init_target = cam.location + cam_direction * (cursor_location - cam.location).length
        #self.init_distance = (cam.location - self.init_target).length

        # Calculate the distance between the camera and the 3D cursor
        initial_distance = (cam.location - cursor_location).length

        # get the direction cming out from the centre of camera
        direction = cam.matrix_world.to_3x3().col[2].normalized()

        virtual_pivot = cam.location - (initial_distance * direction)

        vector_a = cam.location - cursor_location
        vector_b = cam.location - virtual_pivot

        angle = calculate_angle(vector_a, vector_b)
        self.init_distance = (math.cos(angle) * vector_a).length

        # Store the initial focal length
        self.init_focal_length = cam.data.lens

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}
    
    def restore_camera(self, context):
        # Restore the original location of the camera
        context.scene.camera.location = self.original_location
        context.scene.camera.data.lens = self.original_lens

class TruckShift(bpy.types.Operator):
    """Adjust Camera Shift X and Position"""
    bl_idname = "camera.shift_x_adjust"
    bl_label = "Truck Shift"
    bl_options = {'REGISTER', 'UNDO'}

    def modal(self, context, event):
        if event.type == 'MOUSEMOVE':
            # Get the camera
            cam = context.scene.camera
            if cam is None or cam.type != 'CAMERA':
                self.report({'WARNING'}, "No active camera found")
                return {'CANCELLED'}

            cursor_location = bpy.context.scene.cursor.location
            
            distance = (cam.location - cursor_location).length
            
            # Change shift X based on mouse movement
            delta = (event.mouse_x - self.init_mouse_x) * 0.001
            new_shift_x = self.init_shift_x + delta

            # Calculate new camera location to maintain the perceived framing
            new_location = self.init_location - self.cam_x_axis * delta * cam.data.lens * (2/distance)

            # Update the camera properties
            cam.data.shift_x = new_shift_x
            cam.location = new_location

            return {'RUNNING_MODAL'}

        elif event.type in {'RIGHTMOUSE', 'ESC'}:
            return {'CANCELLED'}

        return {'PASS_THROUGH'}

    def invoke(self, context, event):
        self.init_mouse_x = event.mouse_x

        # Get the camera
        cam = context.scene.camera
        if cam is None or cam.type != 'CAMERA':
            self.report({'WARNING'}, "No active camera found")
            return {'CANCELLED'}

        # Store the initial shift X and location
        self.init_shift_x = cam.data.shift_x
        self.init_location = cam.location.copy()

        # Calculate the camera's local X axis
        cam_matrix = cam.matrix_world.to_3x3().transposed()
        self.cam_x_axis = cam_matrix[0].copy()

        context.window_manager.modal_handler_add(self)
        return {'RUNNING_MODAL'}

def menu_func(self, context):
    self.layout.operator(DollyZoom.bl_idname)
    self.layout.operator(TruckShift.bl_idname)

def register():
    bpy.utils.register_class(DollyZoom)
    bpy.utils.register_class(TruckShift)
    bpy.types.VIEW3D_MT_view.append(menu_func)

def unregister():
    bpy.utils.unregister_class(DollyZoom)
    bpy.utils.unregister_class(TruckShift)
    bpy.types.VIEW3D_MT_view.remove(menu_func)

if __name__ == "__main__":
    register()
